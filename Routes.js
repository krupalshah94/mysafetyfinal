import React, { Component } from 'react';
import {BrowserRouter as Router,Route} from 'react-router-dom';
//components
import App from './App';
import ParentSingin from './components/ParentSingin';
import Singup from './components/Singup';
import Blog from './components/Blog details/Blog';
import Blogdetails from './components/Blog details/Blogdetails';
import ChildSingup from './components/ChildSingup';
import Index from './components/Parent Pages/index';
import Order from './components/Parent Pages/order';
class Routes extends Component {

    render() {
    
        return (
            <div>
           
            <Router>
                <ul>
                <Route exact path='/' component={App} />
                <Route exact path='/parentSingin' component={ParentSingin}/>
                <Route exact path='/singup' component={Singup}/>
                <Route exact path='/header' component={App} />
                <Route exact path='/blog' component={Blog} />
                <Route exact path='/features' component={App}/>
                <Route exact path='/pricing' component={App}/>
                <Route exact path='/blogdetails' component={Blogdetails} />
                <Route exact path='/childsingup' component={ChildSingup} />
                <Route exact path='/index' component={Index} />
                <Route exact path='/order' component={Order} />
                </ul>
            </Router>
            </div>

        );
    }
}


export default Routes;