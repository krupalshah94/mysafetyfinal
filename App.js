import React, { Component } from 'react';
import './assets/css/style.css';

//components
import Header from './components/Header';
import Hero from './components/Hero';
import Features from './components/Features';
import Price from './components/Price';
import Blog from './components/Blog';
import Freetrial from './components/Freetrial';
import Footer from './components/Footer';

class App extends Component {
  render() {
    return (
      
      <div className="main-wrapper home-13 layout-strong">
          <Header/>
          <Hero/>
          <Features/>
          <Price/>
          <Blog/>
          <Freetrial/>
          <Footer/>
     
      </div>
     
    );
  }
}

export default App;
