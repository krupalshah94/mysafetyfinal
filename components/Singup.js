import React, { Component } from 'react';
import LoginFooter from './LoginFooter';
import Header from './Header';

class Singup extends Component {
    render() {
        return (
            <div>
                <Header/>
		 {/* <!-- =========== Reviews ============ --> */}
        <section className="page-sign-up user-entrance login-regi-div">
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <div className="section-title">
                            <h2>Parent Sign up / registration</h2>
                            <p>Already have an account?   
                                <a href="#">Login for parent</a>&nbsp;&nbsp;|&nbsp;&nbsp;
								<a href="#">Login for Child</a>
                            </p>
                        </div>
                    </div>
                    {/* <!-- section title end /--> */}
                </div>
                {/* <!-- section title row end --> */}
                <div className="row">
                    <div className="col-12 col-lg-8 mx-auto">
                        <div className="user-form">
                            <form className="form sign-up__form" action="#">
                                <div className="form__field form__field--group">
                                    <label>Parent/Legal Guardian Name</label>
                                    <input className="form-control" type="text" name="fname" value="" placeholder="Enter your full name" required/>
                                </div>
                                <div className="form__field form__field--group">
                                    <label>Parent's mobile number</label>
                                    <input className="form-control" type="text" name="text" value="" placeholder="Enter mobile number" required/>
                                </div>
								 <div className="form__field form__field--group">
                                    <label>Parent's e-mail</label>
                                    <input className="form-control" type="email" name="email" value="" placeholder="Enter email address" required/>
                                </div>
								
                                <div className="form__field form__field--group">
                                    <label>Password</label>
                                    <input className="form-control" type="password" name="password" value="" placeholder="Password must be 8 characters long" required/>
                                </div>
                                <div className="form__field form__field--group">
                                    <label>Confirm Password</label>
                                    <input className="form-control" type="password" name="password" value="" placeholder="Retype the same password" required/>
                                </div>
                                <button type="submit" className="btn__submit">Sign up</button>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
    {/* <!-- testimonial row end --> */}
    </section>
    {/* <!-- =========== Reviews ============ --> */}
            <LoginFooter/>
    </div>
        
        );
    }
}

export default Singup;