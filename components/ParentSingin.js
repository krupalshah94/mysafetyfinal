import React, { Component } from 'react';
import axios from 'axios';
import {Link,Redirect} from 'react-router-dom';
import Header from './Header';
import LoginFooter from './LoginFooter';
import {FormErrors}  from '../components/Error/FormError';
const mystyle = {
    color:'red'
};
class ParentSingin extends Component {
    constructor(){
        super();
        this.state = {
            email:'',
            password:'',
            redirect:'false',
            formErrors: {email: '', password: ''},
            emailValid: false,
            passwordValid: false,
            formValid: false
        }
            this.handleChange = this.handleChange.bind(this);
    };
    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value },  
        () => { this.validateField(name, value) });
        console.log(value);
    }
    handleSubmit(e){
        e.preventDefault();
        const email = this.state.email;
        const password = this.state.password;
        console.log(email,password);
        let user = {
            email: this.state.email,
            password: this.state.password,
            user_type: 'PU'
        };
        axios.post('http://13.127.235.254/dev/laravel/mysafety/public/api/parent/login', user)
        .then((result) => {
            let responseJson = result;
            if(responseJson.data.status=== 200){
                    
                sessionStorage.setItem('token', JSON.stringify(responseJson.data.result.token));
                this.setState({redirect:true});
            }else if(responseJson.data.status === 401){
                this.setState({redirect:false});
                alert('invalid email & password');
            }
          });
    }
    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        let passwordValid = this.state.passwordValid;
      
        switch(fieldName) {
          case 'email':
            emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
            fieldValidationErrors.email = emailValid ? '' : ' is invalid';
            break;
          case 'password':
            passwordValid = value.length >= 8;
            fieldValidationErrors.password = passwordValid ? '': ' is must be 8 character long';
            break;
          default:
            break;
        }
        this.setState({formErrors: fieldValidationErrors,
                        emailValid: emailValid,
                        passwordValid: passwordValid
                      }, this.validateForm);
      }
      
      validateForm() {
        this.setState({formValid: this.state.emailValid && this.state.passwordValid});
      }
      errorClass(error) {
        return (error.length === 0 ? '' : 'has-error');
    }

    render() {
    
        if(this.state.redirect === true || sessionStorage.getItem('token')) {
            return <Redirect to='/index' />
          }
      
        return ( 
    <div className="main-wrapper home-13 layout-strong">
    <Header/>
      {/* <!-- =========== Reviews ============ --> */}
    <section className="page-sign-in user-entrance login-regi-div">
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <div className="section-title">
                        <h2>Parent's Login</h2>
                        <p>Don't have an account?
                            <a href="#">Create a account as parent</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                            <a href="#">Create a account as Child</a>
                        </p>
                    </div>
                </div>
                {/* <!-- section title end --> */}
            </div>
            {/* <!-- section title row end --> */}
            <div className="row">
                <div className="col-12 col-lg-8 mx-auto">
                    <div className="user-form">
                        <form className="form sign-up__form" action="#" method="POST" onSubmit={this.handleSubmit.bind(this)}>
                        <FormErrors  formErrors={this.state.formErrors} />
                        <div className="form__field form__field--group">
                                <label>Parent's e-mail</label>
                                <input className="form-control"
                                type="email" name="email"
                                value={this.state.email}
                                placeholder="Enter email address"
                                onChange={this.handleChange} required/>
                            </div>
                            
                            <div className="form__field form__field--group">
                                <label>Password</label>
                                <a className="forgot-password" href="#">Forgot password?</a>
                                <input className="form-control"
                                type="password"
                                name="password"
                                value={this.state.password}
                                placeholder="Password must be 8 characters long"
                                onChange={this.handleChange} required/>
                            </div>
                            <button type="submit"  disabled={!this.state.formValid} className="btn__submit" >Login</button>
                        </form>
                    </div>

                </div>
            </div>
            
            
            <div className="row">
                <div className="col-12 col-lg-8 mx-auto">
                    <div className="another-login-box clearfix">
                    <div className="user-form">
                        <form className="form sign-up__form" action="#">
                            <p>Already have an account for child?</p>
                            <Link to='/childsingup' className="btn_other_submit">Child Login</Link>
                        </form>
                    </div>
                    </div>

                </div>
            </div>
            
        </div>
    </section>
<LoginFooter/>
   
</div>
        );
    }
}


export default ParentSingin;

