import React, { Component } from 'react';
import {Link} from 'react-router-dom';
class Blog extends Component {
    render() {
        return (
            <div className="home-blog home-blog-one blog-main" id="blog">
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <div className="section-title">
                            <h2>Latest news from us</h2>
                            <p>Read latest technology news from our blog
                            </p>
                        </div>
                    </div>
                    {/* <!-- section title end --> */}
                </div>
                {/* <!-- section title row end --> */}

                <div className="row">
                    <div className="col-12 col-md-6">
                        <div className="blog-featured">
                            <article className="article">
                                <a className="article__thumbnail" href="#">
                                    <img src={require('../img/blog-1.jpg')} alt=""/>
                                </a>
                                <div className="article-content">
                                    <div className="article__meta">
                                      

                                        <span className="article__meta--published">Mar 23, 2018</span>
                                    </div>
                                    <a className="article__title" href="#">
                                        For Women Only Your Computer Usage Could Cost You Your Job
                                    </a>

                                    <p>Sitting proudly atop M3565 is the two-storey penthouse. The master bedroom suite is phenomenally
                                        spacious and opens out to a breathtaking plunge pool and bar area that makes the
                                        most of its unrivalled position.</p>
                                    <a href="#" className="db-btn-link">Continue reading
                                        <i className="db-btn__icon db-btn__icon--after nc-icon nc-tail-right"></i>
                                    </a>
                                </div>
                            </article>
                        </div>
                        {/* <!-- blog featured end --> */}
                    </div>
                    <div className="col-12 col-md-6 col-lg-5">
                        <div className="blog-list">
                            <article className="article">
                                <div className="article-content">
                                    <div className="article__meta">
                                        

                                        <span className="article__meta--published">Mar 23, 2018</span>
                                    </div>
                                    <a className="article__title" href="#">
                                        3 Simple Ways To Save A Bunch Of Money When Buying A New Computer
                                    </a>
                                </div>
                            </article>
                        </div>
                        {/* <!-- single list end --> */}
                        <div className="blog-list">
                            <article className="article">
                                <div className="article-content">
                                    <div className="article__meta">
                                        

                                        <span className="article__meta--published">Mar 23, 2018</span>
                                    </div>
                                    <a className="article__title" href="#">
                                        5 Reasons To Choose A Notebook Over A Computer Desktop
                                    </a>
                                </div>
                            </article>
                        </div>
                        {/* <!-- single list end --> */}
                        <div className="blog-list">
                            <article className="article">
                                <div className="article-content">
                                    <div className="article__meta">
                                        

                                        <span className="article__meta--published">Mar 23, 2018</span>
                                    </div>
                                    <a className="article__title" href="#">
                                        Are You Ready To Buy A Home Theater Audio System
                                    </a>
                                </div>
                            </article>
                        </div>
                        {/* <!-- single list end --> */}
                        <div className="blog-list">
                            <article className="article">
                                <div className="article-content">
                                    <div className="article__meta">
                                        

                                        <span className="article__meta--published">Mar 23, 2018</span>
                                    </div>
                                    <a className="article__title" href="#">
                                        An Ugly Myspace Profile Will Sure Ruin Your Reputation
                                    </a>
                                </div>
                            </article>
                        </div>
                        {/* <!-- single list end --> */}
						
						
						<Link to='/blog' className="db-btn-link">More Blogs
                                        <i className="db-btn__icon db-btn__icon--after nc-icon nc-tail-right"></i>
                                    </Link>
                    </div>
                </div>
            </div>
        </div>
 
		
        );
    }
}

export default Blog;