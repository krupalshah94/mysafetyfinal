import React, { Component } from 'react';
import {Link} from 'react-router-dom';
class Header extends Component {
    render() {
        return (
            <header className="navigation navigation__transparent navigation__caps navigation__separate navigation__right navigation__btn-fill inner-navigation navigation__landscape">
        
            <div className="container-full">
                <div className="row">
                    <div className="col-12">
                        <div className="navigation-content">
                            <Link to='/' className="navigation__brand">
                                <img className="navigation-main__logo" src={require('../img/logo.png')} alt="" />
                                <img className="sticky-nav__logo" src={require('../img/logo.png')} alt="sticky logo" />
                            </Link>
                            <button className="navigation__toggler"></button>
                            {/* <!-- offcanvas toggle button --> */}
                            <nav className="navigation-wrapper">
                                <button className="offcanvas__close">✕</button>
                                {/* <!-- offcanvas close button --> */}
                                <ul className="navigation-menu" id="nav">
                                    <li className="navigation-menu__item">
                                       <Link to="/" className="navigation-menu__link">Features</Link>
                                    </li>
                                    <li className="navigation-menu__item">
                                        <Link to="/" className="navigation-menu__link">Pricing</Link>
                                    </li>
									<li className="navigation-menu__item">
                                        <Link to="/" className="navigation-menu__link">Blog</Link>
                                    </li>                                   
                                </ul>
                            </nav>
                            {/* <!-- nav item end --> */}
                            <div className="navigation-button navigation-button-couple">
                                <Link to="/ParentSingin" className="nav-cta-btn more-space navigation-button-couple__trans">Login</Link>
                                <Link to="/singup" className="db-btn nav-cta-btn navigation-button-couple__fill">Signup</Link>
                            </div>

                        </div>
                    </div>
                </div>
                {/* <!-- row end --> */}
            </div>
        </header>
        );
    }
}

export default Header;