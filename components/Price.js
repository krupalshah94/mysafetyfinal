import React, { Component } from 'react';

class Price extends Component {
    render() {
        return (
            <section className="pricing-two" id="pricing">
            <span className="abstract-shape right">
                <img className="svg" src={require('../img/diagonal-shape.svg')} alt="shape" />
            </span>
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <div className="section-title">
                            <h2>Easy Pricing Plans</h2>
                            <p>Etiam fringilla dolor a leo ultrices, a tristique nisi finibus. Morbi maximus nunc erat, faucibus eleifend metus congue molestie.
                            </p>
                        </div>
                    </div>
                    {/* <!-- section title end --> */}
                </div>
              
                <div className="row">
                    <div className="col-12 col-md-3">
                        <div className="prcing-table free reveal">
                            <div className="prcing-header">
                                <span className="prcing-table__plan-name">Free Trial</span>

                                <p>
                                    <span className="value">2 <span className="small-val">Month</span></span>
                                </p>
                            </div>
							<ul className="plan-features">
                                <li className="plan-features__list">&nbsp;</li>
                            </ul>
                            <a href="#" className="db-btn db-btn__type-lg db-btn__outline">Get started now
                                <i className="db-btn__icon db-btn__icon--after nc-icon nc-tail-right"></i>
                            </a>
                        </div>
                    </div>
                    {/* <!-- pricing column end --> */}
                    <div className="col-12 col-md-3">
                        <div className="prcing-table free reveal">
                            <div className="prcing-header">
                                <span className="prcing-table__plan-name">Free Trial</span>
                                <p>
                                    <span className="currency">$</span>
                                    <span className="value">10</span>
                                </p>
                            </div>
                            <ul className="plan-features">
                                <li className="plan-features__list">one time setup fee for free trial, monthly and annual pricing</li>
                            </ul>
                            <a href="#" className="db-btn db-btn__type-lg db-btn__outline">Get started now
                                <i className="db-btn__icon db-btn__icon--after nc-icon nc-tail-right"></i>
                            </a>
                        </div>
                    </div>
                    {/* <!-- pricing column end --> */}
                    <div className="col-12 col-md-3">
                        <div className="prcing-table company focus1 pro reveal">
                            <div className="prcing-header">
                                <span className="prcing-table__plan-name">Introductory offer</span>
								<p>
									<span className="currency1">$</span>
                                    <span className="value1">8<span className="small-val">Month</span></span>
                                </p>
                                <p>
                                    <span className="currency">$</span>
                                    <span className="value">6<span className="small-val">Month</span></span>
                                </p>
                            </div>
<ul className="plan-features">
                                <li className="plan-features__list">&nbsp;</li>
                            </ul>							
                            <a href="#" className="db-btn db-btn__type-lg db-btn__outline">Get started now
                                <i className="db-btn__icon db-btn__icon--after nc-icon nc-tail-right"></i>
                            </a>
                        </div>
                    </div>
                    {/* <!-- pricing column end --> */}
					<div className="col-12 col-md-3">
                        <div className="prcing-table company reveal">
                            <div className="prcing-header">
                                <span className="prcing-table__plan-name">Introductory offer</span>
								 <p>
									<span className="currency1">$</span>
                                    <span className="value1">69<span className="small-val">Month</span></span>
                                </p>
                                <p>
                                    <span className="currency">$</span>
                                    <span className="value">49<span className="small-val">Annual</span></span>
                                </p>
                            </div>
<ul className="plan-features">
                                <li className="plan-features__list">&nbsp;</li>
                            </ul>							
                            <a href="#" className="db-btn db-btn__type-lg db-btn__outline">Get started now
                                <i className="db-btn__icon db-btn__icon--after nc-icon nc-tail-right"></i>
                            </a>
                        </div>
                    </div>
                    {/* <!-- pricing column end --> */}
                </div>
            </div>
        </section>
        );
    }
}

export default Price;