import React, { Component } from 'react';

class Features extends Component {
    render() {
        return (
            <div>
            <section className="features-four" id="feature">
            <div className="container">
                {/* <!-- section title row end --> */}
                <div className="row">
                    <div className="col-12">
                        <div className="feature-wrapper">
                            <ul className="feature">
                                <li className="feature__list">
                                    <span className="feature__icon">
                                        <i className="nc-icon monitor-icon"></i>
                                    </span>
                                    <div>
									<h4 className="feature__title">Monitor</h4>
                                        <p className="feature__description">Protect your kids without spying or blocking</p>
                                    </div>
                                </li>
                                {/* <!-- single item end --> */}
                                <li className="feature__list">
                                    <span className="feature__icon">
                                        <i className="nc-icon rick-det-icon"></i>
                                    </span>
                                    <div>
                                        <h4 className="feature__title">Risk Detection</h4>
                                        <p className="feature__description">Online activity constantly assessed for dangers</p>
                                    </div>
                                </li>
                                {/* <!-- single item end --> */}
                                <li className="feature__list">
                                    <span className="feature__icon">
                                        <i className="nc-icon parentaler-icon"></i>
                                    </span>
                                    <div>
                                        <h4 className="feature__title">Parent Alert</h4>
                                        <p className="feature__description">Real time threat alerts for parents, so you can take action
                                        </p>
                                    </div>
                                </li>
                                {/* <!-- single item end --> */}
								<li className="feature__list">
                                    <span className="feature__icon">
                                        <i className="nc-icon education-icon"></i>
                                    </span>
                                    <div>
                                        <h4 className="feature__title">Education</h4>
                                        <p className="feature__description">Encouraging open discussions between parents and kids
                                        </p>
                                    </div>
                                </li>
                                {/* <!-- single item end --> */}
                            </ul>
                            {/* <!-- feature content end --> */}
                            <div className="features-cta reveal">
                                <h2 className="features-cta__title">Starting with App is easier than anything!</h2>
                                <p className="features-cta__description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean magna lorem, mattis et orci at, maximus imperdiet enim. Nunc hendrerit condimentum lacus, eget euismod nunc viverra ac.</p>
                                <div className="features-cta-buttons">
                                    <a href="#" className="db-btn db-btn__type-lg db-btn__primary">Start free trial
                                        <i className="db-btn__icon db-btn__icon--after nc-icon nc-tail-right"></i>
                                    </a>
                                    <a href="#" className="db-btn db-btn__type-lg db-btn__outline">
                                        See all features
                                    </a>
                                </div>
                            </div>
                            {/* <!-- media content end --> */}
                        </div>
                    </div>
                </div>
                {/* <!-- row end --> */}
            </div>
        </section>
        <div className="highlight-bg-color">
        {/* <!-- =========== switchable content start ============ --> */}
        <section className="switchable switchable-2">
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <div className="switchable-wrapper">
                            <div className="switchable-content">
                                <h2 className="switchable__title">Backed By Research</h2>
                                <p className="highlight_description">Nullam varius accumsan pulvinar. Proin luctus mauris purus, sit amet rutrum dolor blandit id. Aliquam magna lorem, rhoncus vel bibendum quis, venenatis ut mauris.</p>
								<p className="switchable__description">Praesent condimentum mauris diam, vitae sagittis enim ullamcorper non. Cras lacinia, tellus ut finibus vestibulum, ex ante tristique sem, eget egestas est enim at ligula. Vivamus venenatis vitae justo rutrum elementum. Nullam eget risus in nibh aliquet lobortis ac non neque. Ut vehicula diam sem, ut eleifend mi ullamcorper sed.
                                </p>
                                <div className="switchable-cta-buttons">
                                    <a href="#" className="db-btn db-btn__type-lg db-btn__primary">Start free trial
                                        <i className="db-btn__icon db-btn__icon--after nc-icon nc-tail-right"></i>
                                    </a>
                                    <a href="#" className="db-btn db-btn__type-lg db-btn__outline">
                                        See all features
                                    </a>
                                </div>
                            </div>
                            {/* <!-- content end --> */}
                            <div className="switchable-media reveal">
                                <picture className="switchable-media__img">
                                    <img src={require('../img/phone-1.png')} alt="" />
                                </picture>
                                {/* <!-- media end --> */}
                            </div>
                        </div>
                    </div>
                </div>
                {/* <!-- row end --> */}
            </div>
        </section>
        {/* <!-- =========== switchable content End ============ --> */}

        {/* <!-- =========== switchable content start ============ --> */}
        <section className="switchable switchable-2 reverse">
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <div className="switchable-wrapper">
                            <div className="switchable-content">
                                <h2 className="switchable__title">Complete Protection</h2>
                                <p className="switchable__description">Praesent condimentum mauris diam, vitae sagittis enim ullamcorper non. Cras lacinia, tellus ut finibus vestibulum, ex ante tristique sem, eget egestas est enim at ligula. Vivamus venenatis vitae justo rutrum elementum. Nullam eget risus in nibh aliquet lobortis ac non neque. Ut vehicula diam sem, ut eleifend mi ullamcorper sed.
                                </p>
                                <div className="switchable-cta-buttons">
                                    <a href="#" className="db-btn db-btn__type-lg db-btn__primary">Start free trial
                                        <i className="db-btn__icon db-btn__icon--after nc-icon nc-tail-right"></i>
                                    </a>
                                </div>
                            </div>
                            {/* <!-- content end --> */}
                            <div className="switchable-media reveal">
                                <picture className="switchable-media__img">
                                    <img src={require('../img/phone-2.png')} alt="" />
                                </picture>
                                {/* <!-- media end --> */}
                            </div>
                        </div>
                    </div>
                </div>
                {/* <!-- row end --> */}
            </div>
        </section>
        {/* <!-- =========== switchable content End ============ --> */}
</div>
<section className="working-process" id="working-process">
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <div className="section-title">
                            <h2>How App works?</h2>
                            <p>In under ten minutes set up your whole family for ongoing real-time protection</p>
                        </div>
                    </div>
                    {/* <!-- section title end --> */}
                </div>
                {/* <!-- section title row end --> */}
                <div className="row">
                    <div className="col-12">
                        <div className="working-process-wrapper reveal">
                            <ul className="working-process-list">
                                <li className="working-process-list-item">
                                    <span className="working-process__icon">
                                        <img src={require('../img/signup-icon.png')} alt="" />
                                    </span>
                                    <div>
                                        <h5 className="working-process__title">Sign Up</h5>
                                        <p className="working-process__description">Sign-up for an account online and add your family's details.
                                        </p>
                                    </div>
                                </li>
                                {/* <!-- single item end --> */}
                                <li className="working-process-list-item">
                                    <span className="working-process__icon">
                                         <img src={require('../img/download-icon.png')} alt="" />
                                    </span>
                                    <div>
                                        <h5 className="working-process__title">Download</h5>
                                        <p className="working-process__description">Download the app and login using your unique account details.
                                        </p>
                                    </div>
                                </li>
                                {/* <!-- single item end --> */}
                                <li className="working-process-list-item">
                                    <span className="working-process__icon">
                                        <img src={require('../img/add-devices.png')} alt="" />
                                    </span>
                                    <div>
                                        <h5 className="working-process__title">Add Devices</h5>
                                        <p className="working-process__description">Setup and link the family devices to your account.
                                        </p>
                                    </div>
                                </li>
                                {/* <!-- single item end --> */}
							  <li className="working-process-list-item">
                                    <span className="working-process__icon">
                                        <img src={require('../img/protect-icon.png')}  alt="" />
                                    </span>
                                    <div>
                                        <h5 className="working-process__title">Protect</h5>
                                        <p className="working-process__description">The family's supported devices are ready to be protected in real time from online threats.
                                        </p>
                                    </div>
                                </li>
                                {/* <!-- single item end --> */}
                            </ul>
                        </div>
                    </div>
                </div>
                {/* <!-- content row end --> */}
            </div>
        </section>
</div>

        );
    }
}

export default Features;