import React, { Component } from 'react';
import {Link} from 'react-router-dom';

class Header extends Component {
    render() {
        return (
            <div class="main-wrapper home-13 layout-strong">
            {/* <!-- =========== Navigation Start ============ --> */}
            
            <header class="navigation navigation__transparent navigation__caps navigation__separate navigation__right navigation __btn-fill inner-navigation navigation__landscape sticky-nav">
                <div class="container-full">
                    <div class="row">
                        <div class="col-12">
                            <div class="navigation-content">
                                <Link to='../../' href="index.html" class="navigation__brand">
                                    <img class="navigation-main__logo" src={require('../../img/logo.png')} alt=""/>
                                    <img class="sticky-nav__logo" src={require('../../img/logo.png')} alt="sticky logo"/>
                                </Link>
                                <button class="navigation__toggler"></button>
                                {/* <!-- offcanvas toggle button --> */}
                                <nav class="navigation-wrapper">
                                    <button class="offcanvas__close">✕</button>
                                    {/* <!-- offcanvas close button --> */}
                                    <ul class="navigation-menu" id="nav">
                                        <li class="navigation-menu__item desktop-hidden">
                                            <a class="navigation-menu__link" href="#">Mr. Josh Smith</a>
                                        </li> 
                                        <li class="navigation-menu__item">
                                            <a class="navigation-menu__link active " href="add-child.html">Add child</a>
                                        </li>
                                        <li class="navigation-menu__item">
                                            <Link to='/order' class="navigation-menu__link " >orders</Link>
                                        </li>
                                        <li class="navigation-menu__item no-boder">
                                            <a class="navigation-menu__link" href="subscripitions.html">subscripitions</a>
                                        </li>
                                        <li class="navigation-menu__item desktop-hidden dividar"></li> 
                                        <li class="navigation-menu__item  desktop-hidden"><a class="navigation-menu__link" href="#">Change Password</a></li>
                                        <li class="navigation-menu__item  desktop-hidden"><a class="navigation-menu__link" href="#">Notification (2)</a></li>
                                        <li class="navigation-menu__item  desktop-hidden"><a class="navigation-menu__link" href="#">My Account</a></li>
                                        
                                        <li class="navigation-menu__item desktop-hidden">
                                            <a class="navigation-menu__link" href="#">Logout</a>
                                        </li> 
                                        
                                    </ul>
                                </nav>
                                {/* <!-- nav item end --> */}
                                <div class="navigation-button navigation-button-couple m-hidden">								
                                     <ul class="navigation-menu" id="nav">
                                        <li class="navigation-menu__item">
                                            <a class="db-btn nav-cta-btn navigation-button-couple__fill" href="#feature">Username</a>
                                            <ul class="navigation-dropdown">
                                                <li class="navigation-menu__item"><a class="navigation-menu__link" href="changepassword.html">Change Password</a></li>
                                                <li class="navigation-menu__item"><a class="navigation-menu__link" href="notification.html">Notification (2)</a></li>
                                                <li class="navigation-menu__item"><a class="navigation-menu__link" href="myaccount.html">My Account</a></li>
                                                <li class="navigation-menu__item"><a class="navigation-menu__link" href="#">Logout</a></li>
                                            </ul>
                                        </li>                           
                                    </ul>
                                </div>
                                
                                
                                
    
                            </div>
                        </div>
                    </div>
                    {/* <!-- row end --> */}
                </div>
            </header>
    
        </div>
        
        );
    }
}

export default Header;