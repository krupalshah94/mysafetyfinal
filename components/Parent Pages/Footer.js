import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
            <div>
                
            {/* <!-- =========== footer-2 End ============ --> */}
            <footer class="footer-three">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="footer">
                                <div class="footer-nav-wrapper">
                                    <nav class="footer-widget">
                                        <h5>Our App</h5>
                                        <ul>
                                            <li>
                                                <a href="index.html#feature">Features</a>
                                            </li>
                                            <li>
                                                <a href="index.html#pricing">Pricing</a>
                                            </li>
                                            <li>
                                                <a href="index.html#blog">Blog</a>
                                            </li>
                                        </ul>
                                    </nav>
                                    {/* <!-- widget end --> */}
                                    <nav class="footer-widget">
                                        <h5>Support</h5>
                                        <ul>
                                            <li>
                                                <a href="sign-in.html">Portal Login</a>
                                            </li>
                                            <li>
                                                <a href="sign-in.html">Child Login</a>
                                            </li>
                                            <li>
                                                <a href="#">Support</a>
                                            </li>
                                            <li>
                                                <a href="contact.html">Contact Us</a>
                                            </li>
                                        </ul>
                                    </nav>
                                    {/* <!-- widget end --> */}
                                    <nav class="footer-widget">
                                        <h5>Company</h5>
                                        <ul>
                                            <li>
                                                <a href="about.html">About</a>
                                            </li>
                                            <li>
                                                <a href="privacy.html">Privacy Policy</a>
                                            </li>
                                            <li>
                                                <a href="terms-conditions.html">Terms & Conditions</a>
                                            </li>
                                        </ul>
                                    </nav>
                                    {/* <!-- widget end --> */}
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-bottom">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="footer-bottom-wrapper">
                                     <div class="footer-copyright">
                                        <span class="footer-copyright__text">&copy; 2018 Copyright, All Rights Reserved by MySafetyNet</span>
                                    </div>
                                    <nav class="footer-bottom-nav">
                                        <ul class="footer-social">
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
    
                                    </ul>
                                    </nav>
                                   
                                    {/* <!-- copyright content end --> */}
    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            {/* <!-- =========== footer-2 End ============ --> */}
            </div>
        );
    }
}

export default Footer;