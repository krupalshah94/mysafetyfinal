import React, { Component } from 'react';
import Header from './Header';
import Footer from './Footer';
let style1 = {
    'margin-top': '150px'
};
class Order extends Component {
    render() {
        return (
            <div>
                <Header/>
 <section className="page-sign-in user-entrance gen-form-div">
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <div className="section-title">
                            <h2>Orders</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                        </div>
                    </div>
                    {/* <!-- section title end --> */}
                </div>
                {/* <!-- section title row end --> */}
				
				
				
                <div className="row">
                    <div className="col-12 col-lg-8 mx-auto">
                        
						<div className="order-listing-div clearfix">
						
							<div className="order-listing-row clearfix">
								
								<h3>Septemeber</h3>
								<div className="order-listing-box clearfix">
									<div className="order-listing clearfix">
										<a href="#" data-toggle="modal" data-target="#view-child">
										<div className="thumb">
											 <figure>
												<img src="img/boy.jpg" alt="" />
											 </figure> 
										</div>
										
										<div className="txt-div">
											<h4>Child Name</h4>
											<p className="date-txt">09/05/2018 10:50am</p>
											<p className="more-txt">08 year |  Male</p>
										</div>
										
										<div className="arrow-right-div">
											<i className="arrow-icon"></i>
										</div>
										</a>
									</div>
                                    {/* <!-- end of order-listing-div --> */}
									
									<div className="order-listing last clearfix">
										<a href="#" data-toggle="modal" data-target="#view-child">
										<div className="thumb">
											 <figure>
												<img src="img/girl.jpg" alt="" />
											 </figure> 
										</div>
										
										<div className="txt-div">
											<h4>Child Name</h4>
											<p className="date-txt">09/05/2018 10:50am</p>
											<p className="more-txt">08 year |  Male</p>
										</div>
										
										<div className="arrow-right-div">
											<i className="arrow-icon"></i>
										</div>
										</a>
									</div>
                                    {/* <!-- end of order-listing-div --> */}
									
									<div className="order-listing clearfix">
										<a href="#" data-toggle="modal" data-target="#view-child">
										<div className="thumb">
											 <figure>
												<img src="img/girl.jpg" alt="" />
											 </figure> 
										</div>
										
										<div className="txt-div">
											<h4>Child Name</h4>
											<p className="date-txt">09/05/2018 10:50am</p>
											<p className="more-txt">08 year |  Male</p>
										</div>
										
										<div className="arrow-right-div">
											<i className="arrow-icon"></i>
										</div>
										</a>
									</div>
                                    {/* <!-- end of order-listing --> */}
								
								</div>
                                {/* <!-- end of order-listing-box --> */}
							
							</div>
                            {/* <!-- end of order-listing-row -->							 */}
							
							
							<div className="order-listing-row clearfix">
								
								<h3>August</h3>
								<div className="order-listing-box clearfix">
									<div className="order-listing clearfix">
										<a href="#" data-toggle="modal" data-target="#view-child">
										<div className="thumb">
											 <figure>
												<img src="img/boy.jpg" alt="" />
											 </figure> 
										</div>
										
										<div className="txt-div">
											<h4>Child Name</h4>
											<p className="date-txt">09/05/2018 10:50am</p>
											<p className="more-txt">08 year |  Male</p>
										</div>
										
										<div className="arrow-right-div">
											<i className="arrow-icon"></i>
										</div>
										</a>
									</div>
                                    {/* <!-- end of order-listing-div --> */}
									
									<div className="order-listing last clearfix">
										<a href="#" data-toggle="modal" data-target="#view-child">
										<div className="thumb">
											 <figure>
												<img src="img/girl.jpg" alt="" />
											 </figure> 
										</div>
										
										<div className="txt-div">
											<h4>Child Name</h4>
											<p className="date-txt">09/05/2018 10:50am</p>
											<p className="more-txt">08 year |  Male</p>
										</div>
										
										<div className="arrow-right-div">
											<i className="arrow-icon"></i>
										</div>
										</a>
									</div>
                                    {/* <!-- end of order-listing-div --> */}
								
								</div>
                                {/* <!-- end of order-listing-box --> */}
							
							</div>
                            {/* <!-- end of order-listing-row --> */}
							
							<div className="order-listing-row clearfix">
								
								<h3>July</h3>
								<div className="order-listing-box clearfix">
									<div className="order-listing clearfix">
										<a href="#" data-toggle="modal" data-target="#view-child">
										<div className="thumb">
											 <figure>
												<img src="img/boy.jpg" alt="" />
											 </figure> 
										</div>
										
										<div className="txt-div">
											<h4>Child Name</h4>
											<p className="date-txt">09/05/2018 10:50am</p>
											<p className="more-txt">08 year |  Male</p>
										</div>
										
										<div className="arrow-right-div">
											<i className="arrow-icon"></i>
										</div>
										</a>
									</div>
                                    {/* <!-- end of order-listing-div --> */}
									
									<div className="order-listing last clearfix">
										<a href="#" data-toggle="modal" data-target="#view-child">
										<div className="thumb">
											 <figure>
												<img src="img/girl.jpg" alt="" />
											 </figure> 
										</div>
										
										<div className="txt-div">
											<h4>Child Name</h4>
											<p className="date-txt">09/05/2018 10:50am</p>
											<p className="more-txt">08 year |  Male</p>
										</div>
										
										<div className="arrow-right-div">
											<i className="arrow-icon"></i>
										</div>
										</a>
									</div>
                                    {/* <!-- end of order-listing-div --> */}
								
								</div>
                                {/* <!-- end of order-listing-box --> */}
							
							</div>
                            {/* <!-- end of order-listing-row --> */}
							
							<div className="order-listing-row clearfix">
								
								<h3>June</h3>
								<div className="order-listing-box clearfix">
									<div className="order-listing clearfix">
										<a href="#" data-toggle="modal" data-target="#view-child">
										<div className="thumb">
											 <figure>
												<img src="img/boy.jpg" alt="" />
											 </figure> 
										</div>
										
										<div className="txt-div">
											<h4>Child Name</h4>
											<p className="date-txt">09/05/2018 10:50am</p>
											<p className="more-txt">08 year |  Male</p>
										</div>
										
										<div className="arrow-right-div">
											<i className="arrow-icon"></i>
										</div>
										</a>
									</div>
                                    {/* <!-- end of order-listing-div --> */}
									
									<div className="order-listing last clearfix">
										<a href="#" data-toggle="modal" data-target="#view-child">
										<div className="thumb">
											 <figure>
												<img src="img/girl.jpg" alt="" />
											 </figure> 
										</div>
										
										<div className="txt-div">
											<h4>Child Name</h4>
											<p className="date-txt">09/05/2018 10:50am</p>
											<p className="more-txt">08 year |  Male</p>
										</div>
										
										<div className="arrow-right-div">
											<i className="arrow-icon"></i>
										</div>
										</a>
									</div>
                                    {/* <!-- end of order-listing-div --> */}
								
								</div>
                                {/* <!-- end of order-listing-box --> */}
							
							</div>
                            {/* <!-- end of order-listing-row --> */}
							
							
							
							
						
						</div>
                        {/* <!-- end of order-listing-div --> */}
						

                    </div>
                </div>
				
				{/* <!-- Modal --> */}
<div className="modal fade" style={style1} id="view-child" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div className="modal-dialog modal-lg" role="document">
    <div className="modal-content">
      <div className="modal-header">
      
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div className="modal-body">
        
		<div className="pop-child-detail-div clearfix">
					<div className="pic">
						<figure><img src="img/girl.jpg" alt="" /></figure> 
					</div>
					<div className="txt-div">
						<h4>Sophia William John</h4>
						<p className="badge-txt">Badge ID: #4546556</p>
						
						<div className="pop-child-detail clearfix">
							<div className="container-fluid">
							<div className="row">
							
							
							<div className="col-12 col-lg-3">
								<p className="light-txt">Age:</p>
							</div>
							<div className="col-12 col-lg-9">
								<p className="dark-txt">10 year</p>
							</div>
							
							<div className="col-12 col-lg-3">
								<p className="light-txt">Gender:</p>
							</div>
							<div className="col-12 col-lg-9">
								<p className="dark-txt">Female</p>
							</div>
							
							<div className="col-12 col-lg-3">
								<p className="light-txt">Birth Date:</p>
							</div>
							<div className="col-12 col-lg-9">
								<p className="dark-txt">02/05/2008</p>
							</div>
							
							<div className="col-12 col-lg-3">
								<p className="light-txt">Phone Number:</p>
							</div>
							<div className="col-12 col-lg-9">
								<p className="dark-txt">+1 121 455 5668</p>
							</div>
							
							<div className="col-12 col-lg-3">
								<p className="light-txt">School Name:</p>
							</div>
							<div className="col-12 col-lg-9">
								<p className="dark-txt">Horace Greely High</p>
							</div>
							
							<div className="col-12 col-lg-3">
								<p className="light-txt">School District Number:</p>
							</div>
							<div className="col-12 col-lg-9">
								<p className="dark-txt">#4588899</p>
							</div>
							
							<div className="col-12 col-lg-3">
								<p className="light-txt">State:</p>
							</div>
							<div className="col-12 col-lg-9">
								<p className="dark-txt">New York</p>
							</div>
							
							
							
							</div>
                            {/* !-- end of row -->						 */}
							</div>
                            {/* <!-- end of container-fluid --> */}
						
						</div>
                        {/* <!-- end of pop-child-detail --> */}
						
					</div>
				
				</div>
                {/* <!-- end of pop-child-detail-div --> */}
		
      </div>
      
    </div>
  </div>
</div>
				
				
				
				
				
				
            </div>
        </section>
                <Footer/>
            </div>
        );
    }
}

export default Order;