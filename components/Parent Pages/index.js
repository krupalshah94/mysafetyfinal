import React, { Component } from 'react';
import Header from './Header';
import AddChild from './AddChild';
import Footer from './Footer';
class Index extends Component {
    render() {
        return (
            <div>
                <Header/>
                <AddChild/>
                <Footer/>
            </div>
        );
    }
}

export default Index;