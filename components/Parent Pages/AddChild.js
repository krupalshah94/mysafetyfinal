import React, { Component } from 'react';

class AddChild extends Component {
    render() {
        return (
            <div>
                      {/* <!-- =========== Reviews ============ --> */}
            <section class="page-sign-in user-entrance gen-form-div">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="section-title">
                                <h2>Add child</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                            </div>
                        </div>
                        {/* <!-- section title end --> */}
                    </div>
                    {/* <!-- section title row end --> */}
                    
                    
                    
                    <div class="row">
                        <div class="col-12 col-lg-12 mx-auto">
                            <div class="user-form">
                                <form class="form sign-up__form" action="#">
                                
                                    <div class="row">
                                    
                                        <div class="col-12 col-lg-6">
                                        <div class="form__field form__field--group">
                                            <label>Child First Name</label>
                                            <input class="form-control" type="text" name="text" value="" placeholder="Enter Child First Name" required/>
                                        </div>
                                        </div>
                                        <div class="col-12 col-lg-6">
                                        <div class="form__field form__field--group">
                                            <label>Child Last Name</label>
                                            <input class="form-control" type="text" name="text" value="" placeholder="Enter Child Last Name" required/>
                                        </div>
                                        </div>
                                        {/* <!-- end of col --> */}
                                        
                                        
                                        <div class="col-12 col-lg-6">
                                        <div class="form__field form__field--group">
                                            <label>Age</label>
                                            <input class="form-control" type="text" name="text" value="" placeholder="Enter Child Age" required/>
                                        </div>
                                        </div>
                                        <div class="col-12 col-lg-6">
                                        <div class="form__field form__field--group">
                                            <label>Gender</label>
                                            <div class="form-check">
                                              <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2"/>
                                              <label class="form-check-label" for="exampleRadios2">
                                                Male
                                              </label>
                                            </div>
                                            <div class="form-check">
                                              <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2"/>
                                              <label class="form-check-label" for="exampleRadios2">
                                                Female
                                              </label>
                                            </div>
                                        </div>
                                        </div>
                                        {/* <!-- end of col --> */}
                                        
                                        
                                        <div class="col-12 col-lg-6">
                                        <div class="form__field form__field--group">
                                            <label>Date of Birth</label>
                                                   <div class="input-group date">
                                                 <input type="text" class="form-control"/>
                                                 <span class="input-group-addon">
                                                 <i class="glyphicon glyphicon-th"></i>
                                                 </span>
                                        </div>
                                        </div>
                                        </div>
                                        <div class="col-12 col-lg-6">
                                        <div class="form__field form__field--group">
                                            <label>Phone Number</label>
                                            <input class="form-control" type="text" name="text" value="" placeholder="Enter Child First Name" required/>
                                        </div>
                                        </div>
                                        {/* <!-- end of col --> */}
                                        
                                        
                                        <div class="col-12 col-lg-6">									
                                        <div class="form__field form__field--group">
                                            <label>School Name</label>
                                            <div class="autocomplete">
                                                <input id="myschool" type="text" class="form-control" name="myschool" placeholder="School Name"/>
                                              </div>
                                        </div>
                                        </div>
                                        <div class="col-12 col-lg-6">
                                        <div class="form__field form__field--group">
                                            <label>School District Number</label>
                                            <input class="form-control" type="text" name="text" value="" placeholder="Enter Child First Name" required/>
                                        </div>
                                        </div>
                                        {/* <!-- end of col -->									 */}
                                        
                                        <div class="col-12 col-lg-6">
                                        <div class="form__field form__field--group">
                                            <label>State</label>
                                            <input id="myState" type="text" class="form-control" name="myState" placeholder="School Name"/>
                                        </div>
                                        </div>
                                        <div class="col-12 col-lg-6">
                                        <div class="form__field form__field--group">
                                            <label>User Name</label>
                                            <input class="form-control" type="text" name="text" value="" placeholder="Enter Child First Name" required/>
                                        </div>
                                        </div>
                                        {/* <!-- end of col*/}
                                        
                                        <div class="col-12 col-lg-6">									
                                        <div class="form__field form__field--group">
                                            <label>Password</label>
                                            <input class="form-control" type="text" name="text" value="" placeholder="Enter Child Last Name" required/>
                                        </div>
                                        </div>
                                        <div class="col-12 col-lg-6">
                                        <div class="form__field form__field--group">
                                            <label>Confirm Password</label>
                                            <input class="form-control" type="text" name="text" value="" placeholder="Enter Child First Name" required/>
                                        </div>
                                        </div>
                                        {/* <!-- end of col --> */}
                                    
                                    
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-12 col-lg-4 mx-auto">
                                            <br />
                                            <button type="submit" class="btn__submit" data-toggle="modal" data-target="#payment">Next</button>
                                        </div>
                                    </div>
                                    
                                </form>
                            </div>
    
                        </div>
                    </div>
                    
                    
                    
                </div>
            </section>
        
            </div>
        );
    }
}

export default AddChild;