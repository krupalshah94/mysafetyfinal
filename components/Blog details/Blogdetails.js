import React, { Component } from 'react';
import Header from '../Header';
import LoginFooter from '../LoginFooter';
class Blogdetails extends Component {
    render() {
        return (
            <div>
                <Header/>
                 <section className="page-sign-in user-entrance gen-form-div">
            <div className="container">
              
			 {/* <!-- =========== Blog body start ============ --> */}
        <section className="blog-main blog-details">
           
            <div className="container">
                <div className="row">
                    <aside className="col-12 col-lg-4 mr-auto">
                            <div className="blog-widgets">
                                <div className="blog-widget widget-popular-posts">
                                    <h6 className="blog-widget__title">Recent Posts</h6>
                                    <ul>
                                        <li>
                                            <div className="post-image">
                                                <a href="#">
                                                    <img src={require('../../img/blog-popular-1.png')} alt="blog-popular-img"/>
                                                </a>
                                            </div>
                                            <div className="post-holder">
                                                <a href="#">For Women Only Your Computer Usage Could Cost You Your Job</a>
                                                <span className="blog-widget__meta">
                                                    Mar 23, 2018</span>
                                            </div>
                                        </li>
                                        {/* <!-- single item end --> */}
                                        <li>
                                            <div className="post-image">
                                                <a href="#">
                                                    <img src={require('../../img/blog-popular-1.png')} alt="blog-popular-img"/>
                                                </a>
                                            </div>
                                            <div className="post-holder">
                                                <a href="#">For Women Only Your Computer Usage Could Cost You Your Job</a>
                                                <span className="blog-widget__meta">
                                                    Mar 23, 2018</span>
                                            </div>
                                        </li>
                                        {/* <!-- single item end --> */}
                                        <li>
                                            <div className="post-image">
                                                <a href="#">
                                                    <img src={require('../../img/blog-popular-1.png')} alt="blog-popular-img"/>
                                                </a>
                                            </div>
                                            <div className="post-holder">
                                                <a href="#">For Women Only Your Computer Usage Could Cost You Your Job</a>
                                                <span className="blog-widget__meta">
                                                    Mar 23, 2018</span>
                                            </div>
                                        </li>
                                        {/* <!-- single item end --> */}
                                        <li>
                                            <div className="post-image">
                                                <a href="#">
                                                    <img src={require('../../img/blog-popular-1.png')} alt="blog-popular-img"/>
                                                </a>
                                            </div>
                                            <div className="post-holder">
                                                <a href="#">For Women Only Your Computer Usage Could Cost You Your Job</a>
                                                <span className="blog-widget__meta">
                                                    Mar 23, 2018</span>
                                            </div>
                                        </li>
                                        {/* <!-- single item end --> */}
                                    </ul>
                                </div>
                               
    
                            </div>
                    </aside>
                    {/* <!-- sidebar end --> */}
        
                    <div className="col-lg-7 col-12">
						
						<div className="blog-content">
                            <article className="article">
                                <a className="article__thumbnail" href="blog-detail.html">
                                    <img src={require('../../img/blog-1.jpg')} alt=""/>
                                </a>
                                <div className="article-content">
                                    <div className="article__meta">
                                        <span className="article__meta--published">Mar 23, 2018</span>
                                    </div>
                                    <a className="article__title" href="blog-detail.html">
                                        For Women Only Your Computer Usage Could Cost You Your Job
                                    </a>

                                    <p>
                                        Can you imagine what we will be downloading in another twenty years? I mean who would have ever thought that you could record sound with digital quality fifty years ago? Now we routinely download whole albums worth of music in a couple of minutes to ipods
                                        that hold thousands of songs in the space the size of a zippo lighter!

                                    </p>
                                    <p> What I wonder is what will we download in the future? We can get degrees on line now, which means that we are actually downloading our future doesn’t it. Sounds kind of funny, but true to say that in the future we will
                                        be downloading our future. Will we download our jobs? I mean more and more people are working at home aren’t they? I could see the next wave of employment being over the internet where you never actually see your
                                        boss or maybe you will see him in downloaded form. That would be kind of weird wouldn’t it? It would be cool in the sense that you could be dressed however you want and you could be making funny faces at the boss
                                        and you wouldn’t even have to be behind his back that is unless you were being downloaded by your boss simultaneously. Weird!
                                    </p>
                                    <blockquote>
                                        <p>We know this in our gut, but what can we do about it? How can we motivate ourselves? One of the most difficult aspects of achieving success is staying motivated over the long haul.</p>
                                    </blockquote>

                                    <p>
                                        Developed by the Intel Corporation, HDCP stands for high-bandwidth digital content protection. As the descriptive name implies, HDCP is all about protecting the integrity of various audio and video content as it travels over a multiplicity of different
                                        types of interfaces. Such data interfaces as GVIF, DVI, and HDMI will all support the functionality of HDCP.
                                    </p>
                                    <h6>Is HDCP Free?</h6>
                                    <p>
                                        No. HDCP requires an authorized license. The license can be obtained through Digital Content Protection, which is a subsidiary of Intel Corporation. Generally, the license can be obtained by filing an application and paying an annual fee. Once the application
                                        is accepted and the user agrees to the terms found in the licensing agreement, the right to make use of HDCP is granted.

                                    </p>

                                    <h6>What Are Some Of The Key Terms of Use For HDCP?</h6>
                                    <p> One key term has to do with the transmission of data to unauthorized receivers. That is, an HDCP protected video source is not allowed to transmit protected content to any receiver that has not be verified to be HDCP
                                        compliant. There is also a restriction on the quality of the content, making sure that the DVD-audio content is equal to or less than CD-audio quality on any non-HDCP digital audio outputs. The licensed operator
                                        also covenants to not use their equipment to produce copies of content, and also to make sure that original content is created within the confines of current content protection requirements. What Type of Devices
                                        Make Use of HDCP?
                                    </p>
                                </div>
                            </article>
                            {/* <!-- single item end --> */}
						</div>
						
						<nav>
                            <ul className="pagination">
                                <li className="pagination-item">
                                    <a className="db-btn" href="#">
                                        Older posts
                                    </a>
                                </li>
                                <li className="pagination-item">
                                    <a className="db-btn" href="#">Newer posts</a>
                                </li>
                            </ul>
                        </nav>
                        {/* <!-- pagination end --> */}
					
					
                     
                        {/* <!-- blog content end --> */}
                    </div>
                </div>
            </div>
        </section>
        {/* <!-- =========== Blog body end ============ --> */}
				
            </div>
            </section>
            <LoginFooter/>
            </div>

        
        );
    }
}

export default Blogdetails;