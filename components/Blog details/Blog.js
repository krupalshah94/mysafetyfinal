import React, { Component } from 'react';
import Header from '../Header';
import LoginFooter from '../LoginFooter';
import {Link} from 'react-router-dom';
class Blog extends Component {
    render() {
        return (
            <div>
                <Header/>
      {/* <!-- =========== Reviews ============ --> */}
    <section className="page-sign-in user-entrance gen-form-div">
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <div className="section-title">
                        <h2>Blog</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                    </div>
                </div>
                {/* <!-- section title end --> */}
            </div>
            {/* <!-- section title row end --> */}
            
            
        {/* <!-- =========== Blog body start ============ --> */}
    <section className="blog-main">
        <div className="container">
            <div className="row">
                <div className="col-lg-7 col-12 mx-auto">
                    <div className="blog-content">
                        <article className="article">
                            <a className="article__thumbnail" href="blog-detail.html">
                                <img src={require('../../img/blog-1.jpg')} alt=""/>
                            </a>
                            <div className="article-content">
                                <div className="article__meta">
                                    <span className="article__meta--published">Mar 23, 2018</span>
                                </div>
                                <a className="article__title" href="blog-detail.html">
                                    For Women Only Your Computer Usage Could Cost You Your Job
                                </a>

                                <p>Sitting proudly atop M3565 is the two-storey penthouse. The master bedroom suite is phenomenally spacious and opens out to a breathtaking plunge pool and bar area that makes the most of its unrivalled position.</p>
                                <Link to="/blogdetails" className="db-btn db-btn__blue db-btn__type-md ">Continue reading</Link>
                            </div>
                        </article>
                        {/* <!-- single item end --> */}

                        <article className="article">
                            <a className="article__thumbnail" href="blog-detail.html">
                                <img src={require('../../img/blog-1.png')}  alt=""/>
                            </a>
                            <div className="article-content">
                                <div className="article__meta">
                                      
                                        <span className="article__meta--published">Mar 23, 2018</span>
                                    </div>
                                <a className="article__title" href="blog-detail.html">
                                    Linux Or Windows Which Is It
                                </a>

                                <p>Sitting proudly atop M3565 is the two-storey penthouse. The master bedroom suite is phenomenally spacious and opens out to a breathtaking plunge pool and bar area that makes the most of its unrivalled position.</p>
                                <Link to="/blogdetails" className="db-btn db-btn__blue db-btn__type-md ">Continue reading</Link>
                            </div>
                        </article>
                        {/* <!-- single item end --> */}

                        <article className="article">
                            <a className="article__thumbnail" href="blog-detail.html">
                                <img src={require('../../img/blog-1.png')}  alt=""/>
                            </a>
                            <div className="article-content">
                                <div className="article__meta">

                                        <span className="article__meta--published">Mar 23, 2018</span>
                                    </div>
                                <a className="article__title" href="blog-detail.html">
                                    5 Reasons To Choose A Notebook Over A Computer Desktop
                                </a>

                                <p>Sitting proudly atop M3565 is the two-storey penthouse. The master bedroom suite is phenomenally spacious and opens out to a breathtaking plunge pool and bar area that makes the most of its unrivalled position.</p>
                                <Link to="/blogdetails" className="db-btn db-btn__blue db-btn__type-md ">Continue reading</Link>
                            </div>
                        </article>
                        {/* <!-- single item end --> */}

                        <article className="article">
                            <a className="article__thumbnail" href="blog-detail.html">
                                <img src={require('../../img/blog-1.png')}  alt=""/>
                            </a>
                            <div className="article-content">
                                <div className="article__meta">
                                      
                                        <span className="article__meta--published">Mar 23, 2018</span>
                                    </div>
                                <a className="article__title" href="blog-detail.html">
                                    Addiction When Gambling Becomes A Problem
                                </a>

                                <p>Sitting proudly atop M3565 is the two-storey penthouse. The master bedroom suite is phenomenally spacious and opens out to a breathtaking plunge pool and bar area that makes the most of its unrivalled position.</p>
                                <Link to="/blogdetails" className="db-btn db-btn__blue db-btn__type-md ">Continue reading</Link>
                            </div>
                        </article>
                        {/* <!-- single item end --> */}

                    </div>
                    {/* <!-- blog content end --> */}
                    <nav>
                        <ul className="pagination">
                            <li className="pagination-item">
                                <a className="db-btn" href="#">
                                    Older posts
                                </a>
                            </li>
                            <li className="pagination-item">
                                <a className="db-btn" href="#">Newer posts</a>
                            </li>
                        </ul>
                    </nav>
                    {/* <!-- pagination end --> */}
                </div>
            </div>
        </div>
    </section>
    {/* <!-- =========== Blog body end ============ --> */}
            
            
        </div>
    </section>
    
    <LoginFooter/>
</div>
        );
    }
}

export default Blog;