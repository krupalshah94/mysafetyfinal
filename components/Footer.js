import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
            <footer className="footer-three">
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <div className="footer">
                            <div className="footer-nav-wrapper">
								<nav className="footer-widget">
                                    <h5>Download App</h5>
                                    <ul>
                                        <li>
                                            <a href="#"><img src={require('../img/app-store.png')} alt=""/></a>
                                        </li>
                                        <li>
                                            <a href="#"><img src={require('../img/google-store.png')} alt=""/></a>
                                        </li>
                                    </ul>
                                </nav>
                                <nav className="footer-widget">
                                    <h5>Our App</h5>
                                    <ul>
                                        <li>
                                            <a href="index.html#feature">Features</a>
                                        </li>
                                        <li>
                                            <a href="index.html#pricing">Pricing</a>
                                        </li>
										<li>
                                            <a href="index.html#blog">Blog</a>
                                        </li>
                                    </ul>
                                </nav>
                                {/* <!-- widget end --> */}
                                <nav className="footer-widget">
                                    <h5>Support</h5>
                                    <ul>
                                        <li>
                                            <a href="sign-in.html">Portal Login</a>
                                        </li>
                                        <li>
                                            <a href="sign-in.html">Child Login</a>
                                        </li>
                                        <li>
                                            <a href="#">Support</a>
                                        </li>
                                        <li>
                                            <a href="contact.html">Contact Us</a>
                                        </li>
                                    </ul>
                                </nav>
                                {/* <!-- widget end --> */}
                                <nav className="footer-widget">
                                    <h5>Company</h5>
                                    <ul>
                                        <li>
                                            <a href="about.html">About</a>
                                        </li>
                                        <li>
                                            <a href="privacy.html">Privacy Policy</a>
                                        </li>
                                        <li>
                                            <a href="terms-conditions.html">Terms & Conditions</a>
                                        </li>
                                    </ul>
                                </nav>
                                {/* <!-- widget end --> */}
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div className="footer-bottom">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <div className="footer-bottom-wrapper">
								 <div className="footer-copyright">
                                    <span className="footer-copyright__text">&copy; 2018 Copyright, All Rights Reserved by MySafetyNet</span>
                                </div>
                                <nav className="footer-bottom-nav">
                                    <ul className="footer-social">
                                    <li>
                                        <a href="#">
                                            <i className="fa fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i className="fa fa-twitter"></i>
                                        </a>
                                    </li>

                                </ul>
                                </nav>
                               
                                {/* <!-- copyright content end --> */}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
     
        );
    }
}

export default Footer;