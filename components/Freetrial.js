import React, { Component } from 'react';

class Freetrial extends Component {
    render() {
        return (
            <section className="cta-three">
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <div className="cta reveal">
                            <div className="cta-wrapper">
                                <div className="cta-content">
                                    <h3 className="cta__title">Start Free Trial</h3>
                                    <p className="cta__description">No credit card requried</p>
                                </div>
                                <a href="#" className="db-btn db-btn__type-lg db-btn__primary">Get started now
                                    <i className="db-btn__icon db-btn__icon--after nc-icon nc-tail-right"></i>
                                </a>
                            </div>
                            {/* <!-- content end --> */}
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        );
    }
}

export default Freetrial;