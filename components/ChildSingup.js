import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import Header from './Header';
import LoginFooter from './LoginFooter';
class ChildSingup extends Component {
    render() {
        return (
            <div>
                <Header/>
                 <section className="page-sign-in user-entrance login-regi-div">
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <div className="section-title">
                            <h2>Child Login</h2>
                            <p>Don't have an account?
                                <a href="#">Create a account as parent</a>&nbsp;&nbsp;|&nbsp;&nbsp;
								<a href="#">Create a account as Child</a>
                            </p>
                        </div>
                    </div>
                    {/* <!-- section title end --> */}
                </div>
                {/* <!-- section title row end --> */}
                <div className="row">
                    <div className="col-12 col-lg-8 mx-auto">
                        <div className="user-form">
                            <form className="form sign-up__form" action="#">
                                <div className="form__field form__field--group">
                                    <label>Parent's e-mail</label>
                                    <input className="form-control"
                                    type="email" name="email"
                                    value=""
                                    placeholder="Enter email address"
                                    required/>
                                </div>
                                <div className="form__field form__field--group">
                                    <label>Password</label>
                                    <a className="forgot-password" href="#">Forgot password?</a>
                                    <input className="form-control"
                                    type="password"
                                    name="password"
                                    value=""
                                    placeholder="Password must be 8 characters long"
                                    required/>
                                </div>
                                <button type="submit" className="btn__submit">Login</button>
                            </form>
                        </div>

                    </div>
                </div>
				
				
				<div className="row">
                    <div className="col-12 col-lg-8 mx-auto">
						<div className="another-login-box clearfix">
                        <div className="user-form">
                            <form className="form sign-up__form" action="#">
                                <p>Already have an account for parent?</p>
                                <Link to='/parentSingin' className="btn_other_submit">Parent Login</Link>
                            </form>
                        </div>
						</div>

                    </div>
                </div>
				
            </div>
        </section>
        <LoginFooter/>
            </div>
        );
    }
}

export default ChildSingup;