import React, { Component } from 'react';

class Hero extends Component {
    render() {
        return (
            <section className="hero-thirteen hero-text-light content-left" >
            {/* <img src={require('../assets//layout/hero-wave-1.svg')} alt="media-content" className="media__content" /> */}
             <div className="overlay"></div>
            <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <div className="hero">
                                <div className="hero-wrapper">
                                    <div className="hero-content">
                                        <h1 className="hero__title">Keeping your family safe online</h1>
                                        <p className="hero__caption">Helping families approach cyber safety together using real time behavioural analysis, live alerts, research-backed insights and helpful educational resources.</p>
                                        <a href="#" className="db-btn db-btn__type-lg db-btn__primary">Start free trial
                                            <i className="db-btn__icon db-btn__icon--after nc-icon nc-tail-right"></i>
                                        </a>
                                        <span className="additional__feature">Start your 30-day  free trial.</span>
                                    </div>
                                   
    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <span className="hero__wave">
                    <img className="svg hero__wave--svg" src={require('../assets//layout/hero-wave-13.svg')} alt="hero-wave" />
                </span>
            </section>
        );
    }
}

export default Hero;